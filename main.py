import subprocess
import sys
import os


def get_parameter_file_path_from_cli_params():
    if len(sys.argv) < 2:
        raise Exception('Parameter file path missing.')
    
    path = sys.argv[1]
    return path

def get_parameter_file_path_from_env():
    return os.environ.get('VLAN_PAR_FILE_PATH')

def get_parameter_list_of_vlan(path):
    try:
        par_list = [data.strip() for data in open(path, mode='r')]
    except FileNotFoundError as e:
        print('Check the path of the file!')
    temp_list = []
    VLAN_list = []
    cntr = 0
    for i in par_list:
        temp_list.append(i)
        cntr += 1
        if cntr == 4:
            VLAN_list.append(temp_list)
            temp_list = []
            cntr = 0
    VLAN_list.insert(0, ['DHCP'])
    VLAN_list.insert(1, ['Own setup'])
    return VLAN_list


def set_dhcp():
    subprocess.run("netsh interface ip set address \"Ethernet\" dhcp")


def own_ip_setup():
    new_ip = input('Please give the new ip address: ')
    new_subnet = input('Please give the new subnet mask: ')
    new_gateway = input('Please give the new gateway: ')
    subprocess.run(f"netsh interface ip set address \"Ethernet\" static {new_ip} {new_subnet} {new_gateway}")


def change_ip(ip_address, subnet_mask, default_gateway):
    try:
        ret = subprocess.run(f"netsh interface ip set address \"Ethernet\" static {ip_address} {subnet_mask} {default_gateway}")
        if ret.returncode != 0: 
           print('Return code:'+ str(ret.returncode))
           print('stdout:'+ str(ret.stdout))
           print('stderr:'+ str(ret.stderr))
    except Exception as e:
        print(e)


path = get_parameter_file_path_from_cli_params()
print(f'Using parameter file from: {path}')
#path = get_parameter_file_path_from_env()
VLAN_list = get_parameter_list_of_vlan(path)


print('Please select a VLAN from the list with the numbers:')
cntr = 0
for i in VLAN_list:
    cntr += 1
    print(f' {cntr}: {i[0]}')
p = input('')

if p == '1':
    print(f'You selected DHCP.')
elif p == '2':
    print(f'You selected own setup.')
else:
    print(f'Your choosen VLAN is {VLAN_list[(int(p))-1][0]} {VLAN_list[(int(p))-1][1]}.')


if p == '1':
    set_dhcp()
elif p == '2':
    own_ip_setup()
else:
    change_ip(VLAN_list[(int(p))-1][1],VLAN_list[(int(p))-1][2],VLAN_list[(int(p))-1][3])